<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
//use SebastianBergmann\CodeCoverage\Report\Xml\Unit;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'name'
    ];

    protected $appends = [
        'total_quantity',
        'image_path',
    ];
        //protected  $with = ['image_path'];
    public function units()
    {
        return $this->belongsToMany(Unit::class ,'product_unit','product_id','unit_id')
            ->withPivot('amount');
    }

    public function getTotalQuantityAttribute()
    {
        $total=0;
        foreach ($this->units as $unit){
            $total+=$unit['pivot']['amount'] * $unit['modifier'];
        }
        return $total;
    }
    public function image()
    {
        return $this->morphOne(\App\Models\Image::class,'o');
    }
    public function getImagePathAttribute()
    {
        $image = \App\Models\Image::where('o_type',Product::class)->where('o_id',$this['id'])->first();
        if(is_null($image)){
            return '';
        }
        return $image['path'];
    }

   



}
