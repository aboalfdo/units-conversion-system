<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //task_2_1
    public function store(Request $request)
    {
        if($request->o_type == 'user')
        {
            $user = User::find($request->o_id);
            //make sure if user exists
            if($user){
                $user->image()->create([
                    'path'=>$request->path,
                    'description'=>$request->description
                ]);
                return $user;
            }else{
                return response()->json(
                    [
                        'message'=>'Add image to User',
                        'data'=> [
                            'arabic_error'=>'خطأ في البيانات',
                            'english_error'=>'Data Error',
                            ]
                    ]
                );
            }
        }
        elseif($request->o_type == 'product')
        {
            $product = Product::find($request->o_id);
            //make sure if product exists
            if($product)
            {
                $product->image()->create([
                    'path'=>$request->path,
                    'description'=>$request->description
                ]);
                return $product;
            }
            else{
                return response()->json(
                    [
                        'message'=>'Add image to User',
                        'data'=> [
                            'arabic_error'=>'خطأ في البيانات',
                            'english_error'=>'Data Error'
                        ]
                    ]
                );
            }
        }else{
            return response()->json(
                [
                    'message'=>'Add image',
                    'data'=> [
                        'arabic_error'=>'خطأ في نوع المعطيات',
                        'english_error'=>'Error in data type'
                    ]
                ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        //
    }
}
