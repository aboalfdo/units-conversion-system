<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Unit;
use Illuminate\Http\Request;



class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Product::create($request->all());

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    //task_1_2
    //task_2_3
    public function show($product_id)
    {
        $unit_id = request('unit_id') ;
        $product = Product::with('units')->find($product_id) ;
        
        if($product){
           //task_1_3
           if(!is_null($unit_id)){
               $unit=$product['units']->filter(function ($item) use ($unit_id ) {
                       return $item['id']==$unit_id ;
                   })->values()[0]?? null;
               $product['total_quantity_by_unit_id']=is_null($unit)? 0: $unit['pivot']['amount'] * $unit['modifier'];
           }
//           else{
//               $amount=0;
//            foreach ($prod['units'] as $unit){
//                $amount += $unit['pivot']['amount'] * $unit['modifier'];
//            }
//            $prod['total_quantity'] = $amount;
//           }

           return $product;
        }
        else
        {
           return response()->json(
               [
                   'message'=>'Get Product',
                   'data'=> [
                       'arabic_error'=>'خطأ في البيانات',
                       'english_error'=>'Data errors',
                   ]
               ]
           );
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
