<?php

namespace App\Http\Controllers;

use App\Models\Inventory;
use App\Models\Product;
use App\Models\Unit;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    //task_1_1
    public function store(Request $request)
    {
        $request->validate([
            'product_id' =>'required',
            'unit_id' =>'required',
            'amount' => 'required'
        ]);
        $product = Product::find($request->product_id);
        $unit = Unit::find($request->unit_id);
        if($unit && $product){
            $inventory = new Inventory();
            $inventory->product_id = $request->product_id;
            $inventory->unit_id = $request->unit_id;
            $inventory->amount = $request->amount;
            if($inventory->save())
            {
                return response()->json(
                    [
                        'message'=>'Add to Inventory',
                        'data'=> [
                            'arabic_result'=>' تم إضافة منتج',
                            'english_result'=>'Product added successfully to inventory',
                        ]
                    ]
                );
            }
            else{
                return response()->json(
                    [
                        'message'=>'Add to Inventory',
                        'data'=> [
                            'arabic_error'=>'لم يتم إضافة منتج',
                            'english_error'=>'Product NOT added to inventory'
                        ]
                    ]
                );
            }
        }else{
            return response()->json(
                [
                    'message'=>'Add to Inventory',
                    'data'=> [
                        'arabic_error'=>'خطأ في البيانات',
                        'english_error'=>'There are Some errors in added data ',
                    ]
                ]
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
