<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

         \App\Models\User::factory()->create([
             'name' => 'Test User',
             'email' => 'test@example.com',
         ]);

        ////add units
//         \App\Models\Unit::factory()->create([
//             'name' => 'liters',
//             'modifier' => '1.0'
//         ]);
//        \App\Models\Unit::factory()->create([
//            'name' => 'cubic meters',
//            'modifier' => '1.0'
//        ]);
//        \App\Models\Unit::factory()->create([
//            'name' => 'Kg',
//            'modifier' => '1.0'
//        ]);
//        \App\Models\Unit::factory()->create([
//            'name' => 'g',
//            'modifier' => '1000'
//        ]);

        //// add products
//                \App\Models\Product::factory()->create([
//            'name' => 'Milk',
//        ]);
//        \App\Models\Product::factory()->create([
//            'name' => 'Butter',
//        ]);

    }
}
